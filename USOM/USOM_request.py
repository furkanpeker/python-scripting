#!/usr/bin/python3
import requests 

# Now we will a request to USOM and then assign the response of this request to a variable called as response with get() method of requests class;

response=requests.get("https://www.usom.gov.tr/url-list.txt", verify=False)
# in here; to skip the http secure guard we used the verify build-in boolean variable!

# In the usom-list.txt file; there are all malicious domain names that detected by USOM.

# Then we opem the usom file in writing mode;
file=open("usom.txt", "w")
file.write(str(response.content)) # we are writing the request content from the usom-list.txt to usom.txt file.
file.close




