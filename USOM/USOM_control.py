#!/usr/bin/python3
from tkinter import *  # For more detailed information, take a look at 'What_is_TCL_TK_and_tkinter.txt' !
import datetime

def control():
	file=open("usom.txt", "r")
	content=file.read()
	file.close()

	domain=entry1.get()
	today=datetime.datetime.now()

	if str(domain) not in content:
		file=open("log.txt", "a") 
		text=str(domain)+"  is not malicious or harmful.\nDate:"+str(today)+"\n"+"\n"
		file.write(text)
		file.close()

		variable.set("The domain you entered is not malicious/harmful.")


	else:
		if str(domain) == "":
			variable.set("Please do not leave the domain field blank!")
		else: 
			file=open("log.txt", "a")
			text=str(domain)+" is malicious or harmful!\nDate:"+str(today)+"\n"+"\n"
			file.write(text)
			file.close()
		
			variable.set("The domain you entered is malicious/harmful!")



window=Tk() # In here we assigned the tkinter frame to the 'window' variable by using Tk class's constructor to represent the outermost frame of our application.

# let's determine the title of our frame;
window.title("USOM Domain Control")
 
 
# Top frame;
frameT=Frame(window, height=200, width=300)
frameT.pack()
frameB=Frame(window, height=50, width=300)
frameB.pack()

# A button for control function;


# A label for B button;
label1=Label(frameT,text="Enter the Domain Address that will be controled: ", width=70)
label1.place(x=50,y=50)
label1.pack()


# An entry textfield for URL/domain entered by the User;
entry1=Entry(frameT, bg="gray", fg="white")
entry1.place(x=50, y=30)
entry1.pack()


B=Button(frameB,text="Control", command=control, bg="red") 
""" 
Which is the same as;
JButton B=new JButton("Control"); 
B.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
	}
});
"""

B.place(x=50,y=80) 
# Which is the same as 'setBounds()' method in java swing!
B.pack() 
# Which activates the button such as 'frame.getContentPane().addC(button);' in java swing!

 
# A entry textfield for result;
variable=StringVar()

entry2=Entry(frameB,textvariable=variable, width=70)
entry2.place(x=50, y=10)
entry2.pack()

# A label for warning;
label2=Label(frameT, text="Please start with a Capital letter when typing the domain, as some domains can be contained within any other URL!\nOtherwise the result may be wrong!", width=100)
label2.place(x=50, y=100)
label2.pack()



window.mainloop()  # mainloop() is simply a method that maintain to run the main window (frame) that executes what we wish to execute in an application. This way we keep the application window visible on the screen!




 
